import json
import requests
from Data import Links

session = requests.session()


def test_login_IMS():
    login_file = open("E:\\E Drive\\API\\IMS\\LogIn.json", 'r')
    read_params = login_file.read()
    request_json = json.loads(read_params)
    response = session.post(Links.test_IMS_API_Links(), request_json)
    print(response.status_code)
    print(response.text)
    json_data = response.json()
    return json_data['user'][0]['authKey']


def test_appIdLogIn():
    login_file = open("E:\\E Drive\\API\\IMS\\LogIn.json", 'r')
    read_params = login_file.read()
    request_json = json.loads(read_params)
    response = session.post(Links.test_IMS_API_Links(), request_json)
    print(response.status_code)
    print(response.text)
    json_data = response.json()
    authKey = json_data['user'][0]['authKey']
    print("Passed the Token value is: " + authKey)
    return json_data['user'][0]['appId']
