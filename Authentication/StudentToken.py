from Data import Links
from Utilities import payloads
import requests

session = requests.session()


def test_StudentLogIn():
    logInResponse = session.post(Links.test_IMS_API_Links(), payloads.testSignIn())
    print(logInResponse.status_code)
    responseJSON = logInResponse.json()
    print(responseJSON, logInResponse.text)
    sToken = responseJSON['user'][0]['authKey']
    print("Authentication Token Key is: ", sToken)
    appId = responseJSON['user'][0]['appId']
    print("App ID for the above student is: ", appId)


def test_LogInAppID():
    logInResponse = session.post(Links.test_IMS_API_Links(), payloads.testSignIn())
    responseJSON = logInResponse.json()
    return responseJSON['user'][0]['appId']


def test_LogInAuthentication():
    logInResponse = session.post(Links.test_IMS_API_Links(), payloads.testSignIn())
    responseJSON = logInResponse.json()
    return responseJSON['user'][0]['authKey']
