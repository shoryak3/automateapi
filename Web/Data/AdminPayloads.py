def test_AdminLogin():
    return {"action": "adminLogin",
            "email": "admin@gmail.com",
            "password": "123123"
            }


def test_FetchUserDashboard():
    return {"action": "fetchUserDashboard"}


def test_FetchAllOrder():
    return {"rows": 10,
            "limit": 0,
            "action": "fetchAllOrders"}


def test_FetchStates():
    return {"action": "fetchStates"}


def test_FetchEnquiryOverview():
    return {"action": "fetchEnquiryOverview"}


def test_FetchStudentsList():
    return {"action": "fetchStudentsList",
            "role": "4",
            "limit": 0}


# Fetching Students Details using the Student ID.
def test_FetchParticularStudentDetails():
    return {"action": "fetchStudentDetails",
            "id": "5921",
            "role": 4}


def test_FetchStudentBatchFee():
    return {"action": "fetchStudentBatchFee",
            "student_id": "5921"}


def test_FetchStudentConversation():
    return {"action": "fetchConversation",
            "enq_id": "5921"}


def test_FetchStudentDisplayPrograms():
    return {"action": "displayPrograms",
            "studentId": "5921"}


def test_FetchInstituteBatches():
    return {"action": "fetchInstituteBatches"}


def test_FetchSubjects():
    return {"action": "fetchSubjects"}


def test_FetchTestPrograms():
    return {"action": "getTestprograms",
            "status": 1}


def test_FetchBatchStudentList():
    return {"action": "fetchBatchStudentList",
            "batch_id": "4"}


def test_FetchCourses():
    return {"action": "fetchCourses"}


def test_FetchTestProgram():
    return {"action": "getTestprograms",
            "limit": 0}


def test_FetchProgramStudentList():
    return {"action": "fetchProgramStudentList",
            "test_program": "76",
            "status": "all"}


def test_FetchStudentList():
    return {"action": "fetchStudentsList",
            "caller": "students"}


def test_FetchBatch():
    return {"action": "fetchBatch",
            "student_id": 5920}


def test_MarkStudentAttendance():
    return {"action": "markStudentAttendance",
            "batch_id": "27",
            "student_id": "5920"}


def test_FetchManagers():
    return {"action": "getManagers"}


# Data Entry Operator: 3
# Manage Student Enquiry: 6
# Quality Checker: 7
def test_AddManagers():
    return {"action": "addManager",
            "name": "Shorya Kaushik",
            "email": "shoryak3@outlook.com",
            "primary_mobile": "8279660752",
            "role": "7"}


def test_ManagersStatusUpdate():
    return {"action": "changeUserStatus",
            "user_id": 337,
            "changed_status": 0}


def test_ChangePassword():
    return {"studentId": "337",
            "password": "123123",
            "confirm_password": "123123",
            "action": "changePassword"}


def test_UpdateAdminDetails():
    return {"action": "updateAdminDetails",
            "id": "337",
            "name": "HYK123",
            "email": "sas123@gmail.com",
            "role": "7"}


def test_FetchOrder():
    return {"action": "fetchAllOrders",
            "limit": 0}


def test_FetchAttendanceStudentList():
    return {"role": "4",
            "action": "fetchStudentsList",
            "caller": "students"}
