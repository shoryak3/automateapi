def test_LoginPayload():
    return {"action": "login",
            "email": "abc@gmail.com",
            "password": "123123"
            }


def test_ReportCard():
    return {"action": "getReportCardData"}


def test_CourseDetails():
    return {"action": "testPaperDetails",
            "program_id": "64"}


def test_CourseDisplayPrograms():
    return {"action": "displayPrograms"}


def test_FetchUserDetails():
    return {"action": "fetchUserDetails"}


def test_FetchCourses():
    return {"action": "fetchCourses", "is_action": 1}


def test_FetchStudentProfileData():
    return {"action": "fetchStudentProfileData"}


# Study Materials
def test_TestPaperMaterials():
    return {"action": "testPaperDetails",
            "program_id": "3"}


def test_FetchStudyMaterials():
    return {"action": "fetchStudyMaterialsByProgramIdWeb",
            "program_id": "3",
            "type": 1,
            "limit": 20,
            "page": 1}


def test_FetchStudyVideos():
    return {"action": "fetchStudyMaterialsByProgramIdWeb",
            "program_id": "3",
            "type": 2,
            "limit": 20,
            "page": 1}


def test_FetchLeaderBoard():
    return {"action": "fetchLeaderboard",
            "test_paper": "133"}


def test_FetchArticleTags():
    return {"action": "fetchArticletags",
            "is_active": 1}


def test_FetchArticles():
    return {"action": "fetchArticles",
            "status": 1,
            "tags": ["2"]}
