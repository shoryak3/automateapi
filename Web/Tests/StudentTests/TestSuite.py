"""
In the Request versions above 2.4
we have one more parameter json,
using the same we can pass the raw data
"""
from Web.StudentLinks import Links
from Web.Data import Payloads
import requests
session = requests.session()


def test_LogInStudent():
    rLogInStudent = session.post(Links.test_Links(),
                                 json=Payloads.test_LoginPayload())
    print(rLogInStudent.status_code)


def test_StudentReportCards():
    rReportCard = session.post(Links.test_Links(),
                               json=Payloads.test_ReportCard())
    x = rReportCard.json()
    rMessage = x['message']
    print(rReportCard.status_code)
    print(rMessage)


def test_CourseDetails():
    rCourseDetails = session.post(Links.test_Links(),
                                  json=Payloads.test_CourseDetails())
    jsonMessage = rCourseDetails.json()
    sMessageSuccess = jsonMessage['message']
    print(rCourseDetails.status_code)
    print(sMessageSuccess)


def test_MyCourseDisplayPrograms():
    rDisplayPrograms = session.post(Links.test_Links(),
                                    json=Payloads.test_CourseDisplayPrograms())
    print(rDisplayPrograms.status_code)


def test_FetchUserDetails():
    rFetchUserDetails = session.post(Links.test_Links(),
                                     json=Payloads.test_FetchUserDetails())
    print(rFetchUserDetails.status_code)
    sMessage = rFetchUserDetails.json()
    getSuccessMessage = sMessage['message']
    print(getSuccessMessage)


def test_FetchCourses():
    rFetchCourses = session.post(Links.test_Links(),
                                 json=Payloads.test_FetchCourses())
    sMessage = rFetchCourses.json()
    getSuccessMessage = sMessage['message']
    print(getSuccessMessage)


def test_FetchStudentProfileData():
    rFetchStudentProfileData = session.post(Links.test_Links(),
                                            json=Payloads.test_FetchStudentProfileData())
    sMessage = rFetchStudentProfileData.json()
    getSuccessMessage = sMessage['message']
    print(getSuccessMessage)


def test_FetchTestPapers():
    rFetchStudentTestPaper = session.post(Links.test_Links(),
                                          json=Payloads.test_TestPaperMaterials())
    print(rFetchStudentTestPaper.status_code)


def test_FetchStudentStudyMaterials():
    rFetchStudentStudyMaterials = session.post(Links.test_Links(),
                                               json=Payloads.test_FetchStudyMaterials())
    print(rFetchStudentStudyMaterials.status_code)


def test_FetchStudentStudyVideos():
    rFetchStudentStudyVideos = session.post(Links.test_Links(),
                                            json=Payloads.test_FetchStudyVideos())
    print(rFetchStudentStudyVideos.status_code)


def test_FetchLeaderBoard():
    rFetchLeaderBoard = session.post(Links.test_Links(),
                                     json=Payloads.test_FetchLeaderBoard())
    sMessage = rFetchLeaderBoard.json()
    getSuccessMessage = sMessage['message']
    print(getSuccessMessage)


def test_FetchArticleTags():
    rFetchArticleTags = session.post(Links.test_Links(),
                                     json=Payloads.test_FetchArticleTags())
    sMessage = rFetchArticleTags.json()
    getSuccessMessage = sMessage['message']
    print(getSuccessMessage)


def test_FetchArticles():
    rFetchArticles = session.post(Links.test_Links(),
                                  json=Payloads.test_FetchArticles())
    print(rFetchArticles.status_code)
