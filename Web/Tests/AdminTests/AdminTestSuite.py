"""
In the Request versions above 2.4
we have one more parameter json,
using the same we can pass the raw data
"""
from Web.StudentLinks import Links
from Web.Data import AdminPayloads
import requests

session = requests.session()


def test_AdminLogin():
    rAdminLogin = session.post(Links.test_Links(),
                               json=AdminPayloads.test_AdminLogin())
    sEmail = rAdminLogin.json()
    getAdminMail = sEmail['user']['email']
    assert getAdminMail == "admin@gmail.com"
    print("\nAssertion is being passed, Admin mail = ", getAdminMail,
          "\nStatus Code: ", rAdminLogin.status_code)


def test_FetchUserDashboard():
    rFetchUserDashboard = session.post(Links.test_Links(),
                                       json=AdminPayloads.test_FetchUserDashboard())
    sMessage = rFetchUserDashboard.json()
    getSuccessMessage = sMessage['message']
    print("\nSuccess Message: ", getSuccessMessage,
          "\nStatus Code: ", rFetchUserDashboard.status_code)


def test_FetchAllOrders():
    rFetchAllOrders = session.post(Links.test_Links(),
                                   json=AdminPayloads.test_FetchAllOrder())
    sMessage = rFetchAllOrders.json()
    getSuccessMessage = sMessage['message']
    print("\nSuccess Message: ", getSuccessMessage,
          "\nStatus Code: ", rFetchAllOrders.status_code)


def test_FetchStates():
    rFetchStates = session.post(Links.test_Links(),
                                json=AdminPayloads.test_FetchStates())
    sMessage = rFetchStates.json()
    getSuccessMessage = sMessage['message']
    print("\nSuccess Message: ", getSuccessMessage,
          "\nStatus Code: ", rFetchStates.status_code)


def test_FetchEnquiryOverview():
    rFetchEnquiryOverview = session.post(Links.test_Links(),
                                         json=AdminPayloads.test_FetchEnquiryOverview())
    sMessage = rFetchEnquiryOverview.json()
    getSuccessMessage = sMessage['message']
    print("\nSuccess Message: ", getSuccessMessage,
          "\nStatus Code: ", rFetchEnquiryOverview.status_code)


def test_FetchStudentList():
    rFetchStudentList = session.post(Links.test_Links(),
                                     json=AdminPayloads.test_FetchStudentsList())
    sMessage = rFetchStudentList.json()
    getSuccessMessage = sMessage['message']
    print("\nSuccess Message: ", getSuccessMessage,
          "\nStatus Code: ", rFetchStudentList.status_code)


def test_FetchStudentBatchFee():
    rFetchStudentBatchFee = session.post(Links.test_Links(),
                                         json=AdminPayloads.test_FetchStudentBatchFee())
    sMessage = rFetchStudentBatchFee.json()
    getSuccessMessage = sMessage['message']
    print("\nSuccess Message: ", getSuccessMessage,
          "\nStatus Code: ", rFetchStudentBatchFee.status_code)


def test_FetchStudentConversation():
    rFetchStudentConversation = session.post(Links.test_Links(),
                                             json=AdminPayloads.test_FetchStudentConversation())
    sMessage = rFetchStudentConversation.json()
    getSuccessMessage = sMessage['message']
    print("\nSuccess Message: ", getSuccessMessage,
          "\nStatus Code: ", rFetchStudentConversation.status_code)


def test_FetchStudentDisplayPrograms():
    rFetchStudentDisplayPrograms = session.post(Links.test_Links(),
                                                json=AdminPayloads.test_FetchStudentDisplayPrograms())
    print("\nStatus Code: ", rFetchStudentDisplayPrograms.status_code)


def test_FetchInstituteBatches():
    rFetchInstituteBatches = session.post(Links.test_Links(),
                                          json=AdminPayloads.test_FetchInstituteBatches())
    sMessage = rFetchInstituteBatches.json()
    getSuccessMessage = sMessage['message']
    print("\nData Display: ", getSuccessMessage,
          "\nStatus Code: ", rFetchInstituteBatches.status_code)


def test_FetchSubjects():
    rFetchSubjects = session.post(Links.test_Links(),
                                  json=AdminPayloads.test_FetchSubjects())
    print("\nStatus Code: ", rFetchSubjects.status_code)


def test_FetchTestPrograms():
    rFetchStudentPrograms = session.post(Links.test_Links(),
                                         json=AdminPayloads.test_AdminLogin())
    print("\nStatus Code: ", rFetchStudentPrograms.status_code)


def test_FetchBatchStudentList():
    rFetchBatchStudentList = session.post(Links.test_Links(),
                                          json=AdminPayloads.test_FetchBatchStudentList())
    sMessage = rFetchBatchStudentList.json()
    getSuccessMessage = sMessage['message']
    print("\nData Display: ", getSuccessMessage,
          "\nStatus Code: ", rFetchBatchStudentList.status_code)


def test_FetchCourses():
    rFetchCourses = session.post(Links.test_Links(),
                                 json=AdminPayloads.test_FetchCourses())
    sMessage = rFetchCourses.json()
    getSuccessMessage = sMessage['message']
    print("\nData Display: ", getSuccessMessage,
          "\nStatus Code: ", rFetchCourses.status_code)


def test_FetchTestProgram():
    rFetchTestProgram = session.post(Links.test_Links(),
                                     json=AdminPayloads.test_FetchTestProgram())
    print("\nStatus Code: ", rFetchTestProgram.status_code)


def test_FetchProgramStudentList():
    rFetchProgramStudentList = session.post(Links.test_Links(),
                                            json=AdminPayloads.test_FetchProgramStudentList())
    sMessage = rFetchProgramStudentList.json()
    getSuccessMessage = sMessage['message']
    print("\nData Display: ", getSuccessMessage,
          "\nStatus Code: ", rFetchProgramStudentList.status_code)


def test_FetchStudentLists():
    rFetchStudentLists = session.post(Links.test_Links(),
                                      json=AdminPayloads.test_FetchStudentList())
    sMessage = rFetchStudentLists.json()
    getSuccessMessage = sMessage['message']
    print("\nSuccess Message: ", getSuccessMessage,
          "\nStatus Code: ", rFetchStudentLists.status_code)


def test_FetchBatch():
    rFetchBatch = session.post(Links.test_Links(),
                               json=AdminPayloads.test_FetchBatch())
    sMessage = rFetchBatch.json()
    getSuccessMessage = sMessage['message']
    print("\nSuccess Message: ", getSuccessMessage,
          "\nStatus Code: ", rFetchBatch.status_code)


def test_MarkStudentAttendance():
    rMarkAttendance = session.post(Links.test_Links(),
                                   json=AdminPayloads.test_MarkStudentAttendance())
    sMessage = rMarkAttendance.json()
    getSuccessMessage = sMessage['message']
    if getSuccessMessage == "Error: Attendance Already Marked.":
        print("\nAttendance marked already"
              "\nStatus Code: ", rMarkAttendance.status_code)
    else:
        print("\nSuccess Message: ", getSuccessMessage,
              "\nStatus Code: ", rMarkAttendance.status_code)


def test_FetchManagers():
    rFetchManagers = session.post(Links.test_Links(),
                                  json=AdminPayloads.test_FetchManagers())
    sMessage = rFetchManagers.json()
    getSuccessMessage = sMessage['message']
    print("\nSuccess Message: ", getSuccessMessage,
          "\nStatus Code: ", rFetchManagers.status_code)


def test_AddStaffMembers():
    rAddStaffMembers = session.post(Links.test_Links(),
                                    json=AdminPayloads.test_AddManagers())
    sMessage = rAddStaffMembers.json()
    getMessage = sMessage['message']
    print(getMessage)


def test_ManagerStatusUpdate():
    rManagerStatusUpdate = session.post(Links.test_Links(),
                                        json=AdminPayloads.test_ManagersStatusUpdate())
    sMessage = rManagerStatusUpdate.json()
    getSuccessMessage = sMessage['message']
    print("\nSuccess Message: ", getSuccessMessage,
          "\nStatus Code: ", rManagerStatusUpdate.status_code)


def test_ChangePassword():
    rChangePassword = session.post(Links.test_Links(),
                                   json=AdminPayloads.test_ChangePassword())
    sMessage = rChangePassword.json()
    getMessage = sMessage['message']
    print("\nSuccess Message: ", getMessage,
          "\nStatus Code: ", rChangePassword.status_code)


def test_UpdateAdminDetails():
    rUpdateAdminDetails = session.post(Links.test_Links(),
                                       json=AdminPayloads.test_UpdateAdminDetails())
    sMessage = rUpdateAdminDetails.json()
    getMessage = sMessage['message']
    print("\nSuccess Message: ", getMessage,
          "\nStatus Code: ", rUpdateAdminDetails.status_code)


def test_FetchAllOrdersManagement():
    rFetchAllOrdersManagement = session.post(Links.test_Links(),
                                             json=AdminPayloads.test_FetchAllOrder())
    sMessage = rFetchAllOrdersManagement.json()
    getMessage = sMessage['message']
    print("\nSuccess Message: ", getMessage,
          "\nStatus Code: ", rFetchAllOrdersManagement.status_code)


def test_FetchAttendanceStudentsList():
    rFetchAttendanceStudentsList = session.post(Links.test_Links(),
                                                json=AdminPayloads.test_FetchAttendanceStudentList())
    sMessage = rFetchAttendanceStudentsList.json()
    getMessage = sMessage['message']
    print("\nSuccess Message: ", getMessage,
          "\nStatus Code: ", rFetchAttendanceStudentsList.status_code)
