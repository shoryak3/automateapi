from behave import *
from Web.Data import Payloads
from Web.StudentLinks import Links
import requests

session = requests.session()


@given("student Log in with the desired credentials")
def test_StudentLogin():
    rLogInStudent = session.post(Links.test_Links(),
                                 json=Payloads.test_LoginPayload())
    print(rLogInStudent.status_code)


@When("student fetch profile data & Report Card Details")
def test_StudentFetchData():
    rFetchStudentProfileData = session.post(Links.test_Links(),
                                            json=Payloads.test_FetchStudentProfileData())
    sMessage = rFetchStudentProfileData.json()
    getSuccessMessage = sMessage['message']
    print(getSuccessMessage)

    rReportCard = session.post(Links.test_Links(),
                               json=Payloads.test_ReportCard())
    x = rReportCard.json()
    rMessage = x['message']
    print(rReportCard.status_code)
    print(rMessage)


@Then("student Logs out")
def test_StudentLogouts():
    var = requests.Timeout
    print(var)


def test_Check():
    rFetchStudentProfileData = session.post(Links.test_Links(),
                                            json=Payloads.test_FetchStudentProfileData())
    sMessage = rFetchStudentProfileData.json()
    getSuccessMessage = sMessage['message']
    print(getSuccessMessage)
