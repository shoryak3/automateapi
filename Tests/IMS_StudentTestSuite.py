"""
Sign UP
Sign IN
Forgot Password - May leave for now
Fetching the Articles
Sign UP Link: "http://arthacademy.4thpointer.in/register"

Add Student through admin: "http://arthacademy.4thpointer.in/admin/add-student.php"
"""

from Data import Links
from Utilities import payloads
import requests

session = requests.session()


def test_StudentLogIn():
    logInResponse = session.post(Links.test_IMS_API_Links(), payloads.test_SignInABC())
    print(logInResponse.status_code)
    responseJSON = logInResponse.json()
    print(responseJSON, logInResponse.text)
    sToken = responseJSON['user'][0]['authKey']
    print("Authentication Token Key is: ", sToken)
    appId = responseJSON['user'][0]['appId']
    print("App ID for the above student is: ", appId)
    sMessage = responseJSON['message']
    print(sMessage)


def test_articlesList():
    articlesList_Response = session.post(Links.test_IMS_API_Links(), payloads.fetchArticles())
    print(articlesList_Response.status_code)
    jsonArticleList = articlesList_Response.json()
    sArticleMessage = jsonArticleList['message']
    print(sArticleMessage)


def test_ProfileUpdate():
    rProfileUpdateResponse = session.post(Links.test_IMS_API_Links(), payloads.test_StudentProfile())
    print(rProfileUpdateResponse.status_code)
    jsonProfileUpdateResponse = rProfileUpdateResponse.json()
    print(jsonProfileUpdateResponse)


# def test_CoursesForSale
