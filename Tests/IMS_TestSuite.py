import requests
import json
from Data import Links
from Utilities import payloads


# Creating a Session requests global,
# so that we don't have to create/establish the session again and again with multiple requests.

session = requests.session()

"""All test Scenarios are being covered under the below test only"""


def test_login_IMS():
    login_file = open("E:\\E Drive\\API\\IMS\\LogIn.json", 'r')
    read_params = login_file.read()
    request_json = json.loads(read_params)
    response = session.post(Links.test_IMS_API_Links(), request_json)
    print(response.status_code)
    print(response.text)
    json_data = response.json()
    authKey = json_data['user'][0]['authKey']
    if authKey == "4297f44b13955235245b2497399d7a93":
        print("Passed the Token value is: " + authKey)
    appId = json_data['user'][0]['appId']
    if appId == "339":
        print("The app ID is: " + appId)
    message = json_data['message']
    print("The log in Message is: " + message)


def test_batchList():
    batchListResponse = session.post(Links.test_IMS_API_Links(), payloads.batchList())
    print(batchListResponse.status_code)
    batchList_jsonData = batchListResponse.json()
    print(batchList_jsonData)


def test_StudyMaterialList():
    response3 = session.post(Links.test_IMS_API_Links(), payloads.studyMaterialList())
    print(response3.status_code)
    studyMaterial_jsonData = response3.json()
    print(studyMaterial_jsonData)
    pdf = studyMaterial_jsonData['study_material'][0]['study_material_path']
    print("\n \n \n Pdf File link is :", pdf)


def test_CoursesForSale():
    courses_response = session.post(Links.test_IMS_API_Links(), payloads.coursesSale())
    print(courses_response.status_code)
    assert courses_response.status_code == 200
    courseJson = courses_response.json()
    print(courseJson)
    name = courseJson['data'][0]['name']
    print(name)


def test_BoughtAPI():
    bought_response = session.post(Links.test_IMS_API_Links(), payloads.boughtAPI())
    print(bought_response.status_code)


def test_MarkStudentAttendance():
    attendanceResponse = session.post(Links.test_IMS_API_Links(), payloads.markStudentAttendance())
    print(attendanceResponse.status_code)
    assert attendanceResponse.status_code == 200


def test_RegistrationEnquiry():
    registrationResponse = session.post(Links.test_IMS_API_Links(), payloads.registrationEnquiry())
    print(registrationResponse.status_code)
    assert registrationResponse.status_code == 200


def test_BuyProgram():
    programResponse = session.post(Links.test_IMS_API_Links(), payloads.buyProgram())
    print(programResponse.status_code)
    assert programResponse.status_code == 200


def test_FetchCourse():
    fetchCourse_Response = session.post(Links.test_IMS_API_Links(), payloads.fetchCourse())
    print(fetchCourse_Response.status_code)
    assert fetchCourse_Response.status_code == 200


def test_FetchStudentBatches():
    fetchStudentResponse = session.post(Links.test_IMS_API_Links(), payloads.fetchStudentBatches())
    print(fetchStudentResponse.status_code)
    assert fetchStudentResponse.status_code == 200


def test_TestPaperList():
    testPaperList_Response = session.post(Links.test_IMS_API_Links(), payloads.testPaperList())
    print(testPaperList_Response.status_code)
    assert testPaperList_Response.status_code == 200


def test_MarkAttendance():
    markAttendance_Response = session.post(Links.test_IMS_API_Links(), payloads.markAttendance())
    print(markAttendance_Response.status_code)
    assert markAttendance_Response.status_code == 200


def test_ChangeStudentPassword():
    changeStudentPassword_Response = session.post(Links.test_IMS_API_Links(), payloads.changeStudentPassword())
    print(changeStudentPassword_Response.status_code)
    assert changeStudentPassword_Response.status_code == 200


def test_viewArticles():
    viewArticles_Response = session.post(Links.test_IMS_API_Links(), payloads.viewArticles())
    print(viewArticles_Response.status_code)
    assert viewArticles_Response.status_code == 200


def test_articlesList():
    articlesList_Response = session.post(Links.test_IMS_API_Links(), payloads.articlesList())
    print(articlesList_Response.status_code)
    assert articlesList_Response.status_code == 200


def test_fetchPackageList():
    fetchPackage_Response = session.post(Links.test_IMS_API_Links(), payloads.fetchPackageList())
    print(fetchPackage_Response.status_code)
    assert fetchPackage_Response.status_code == 200


def test_DeleteStudyMaterial():
    deleteStudyMaterial_Response = session.post(Links.test_IMS_API_Links(), payloads.deleteStudyMaterial())
    print(deleteStudyMaterial_Response.status_code)
    assert deleteStudyMaterial_Response.status_code == 200
