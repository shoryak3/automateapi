from Utilities import Token
from Authentication import StudentToken


def testSignIn():
    return {"email": "shoryak3@outlook.com",
            "password": "Test@1234",
            "action": "appLogin",
            "device_id": "test_shorya"
            }


def test_SignInABC():
    return {"email": "abc@gmail.com",
            "password": "123123",
            "action": "appLogin",
            "device_id": "test_shorya"
            }


def batchList():
    return {"authKey": Token.authKey(),
            "appId": Token.appId(),
            "action": "fetchInstituteBatches"
            }


def studyMaterialList():
    return {"authKey": Token.authKey(),
            "appId": Token.appId(),
            "action": "fetchStudyMaterials"
            }


def coursesSale():
    return {"authKey": Token.authKey(),
            "appId": Token.appId(),
            "action": "getTestprogramsAvailableForSave"
            }


def boughtAPI():
    return {"action": "ifBought",
            "program_id": "10",
            "authKey": Token.authKey(),
            "appId": Token.appId(),
            }


def markStudentAttendance():
    return {"action": "markStudentAttendance",
            "student_id": "46",
            "authKey": Token.authKey(),
            "appId": Token.appId(),
            "batch_id": "2"
            }


def registrationEnquiry():
    return {"action": "addEnquiry",
            "name": "Nishita",
            "gender": "2",
            "email": "nnn_test1@gmail.com",
            "primary_mobile": 23412323434,
            "role": 5,
            "secondary_mobile": "",
            "remark": ""
            }


def buyProgram():
    return {"param1": "",
            "param2": "",
            "appId": Token.appId(),
            "authKey": Token.authKey(),
            "status": "success"
            }


def fetchCourse():
    return {"action": "fetchCourseDetails",
            "test_program": "1"
            }


def fetchStudentBatches():
    return {"action": "fetchStudentBatches",
            "student_id": 4,
            "appId": Token.appId(),
            "authkey": Token.authKey()
            }


def testPaperList():
    return {"authKey": Token.authKey(),
            "appId": Token.appId(),
            "action": "testPaperDetails",
            "program_id": 6
            }


def markAttendance():
    return {"authKey": Token.authKey(),
            "appId": Token.appId(),
            "action": "markStudentAttendance",
            "batch_id": 13,
            "student_id": 122
            }


def changeStudentPassword():
    return {"action": "changeProfilePassword",
            "current_password": "8826612862",
            "new_password": "9015574330",
            "confirm_password": "",
            "authKey": Token.authKey(),
            "appId": Token.appId(),
            }


def articlesList():
    return {"appId": Token.appId(),
            "authKey": Token.authKey(),
            "action": "fetchArticles",
            "is_Active": 1
            }


def fetchArticles():
    return {"appId": StudentToken.test_LogInAppID(),
            "authKey": StudentToken.test_LogInAuthentication(),
            "action": "fetchArticles",
            "is_Active": 1
            }


def viewArticles():
    return {"authKey": Token.authKey(),
            "appId": Token.appId(),
            "action": "fetchArticlesDetails",
            "id": 9,
            "device_id": "test_shorya"
            }


def fetchPackageList():
    return {"authKey": Token.authKey(),
            "appId": Token.appId(),
            "action": "displayPrograms"
            }


def deleteStudyMaterial():
    return {"authKey": Token.authKey(),
            "appId": Token.appId(),
            "action": "deleteStudyMaterial",
            "study_Material_id": 1
            }


image = {"media": open('E:\\E Drive\\Repo_API\\IMS\\Data\\IMG20200412182750.jpg', 'r')}


def test_StudentProfile():
    return {"authKey": StudentToken.test_LogInAuthentication(),
            "appId": StudentToken.test_LogInAppID(),
            "action": "studentChangePublicProfile",
            "profile_picture": image
            }
