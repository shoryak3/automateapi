def test_LogIn():
    return {"action": "login",
            "email": "sk@m.com",
            "password": "Test@1234"
            }


def test_ReportCard():
    return {"action": "getReportCardData"}


def test_FetchUserDetails():
    return {"action": "fetchUserDetails"}


def test_FetchCourses():
    return {"action": "fetchCourses"}


def test_GetAlLCoursesForSale():
    return {"action": "getAllCoursesForSale",
            "limit": 10,
            "page": 1,
            "sort_column": "created",
            "sort_order": "ASC",
            "is_free": 1}


def test_GetLatestCourses():
    return {"action": "getLatestCouses"}


def test_GetFreeCourses():
    return {"action": "getFreeCouses"}


def test_getAllCoursesForSale():
    return {"action": "getAllCoursesForSale",
            "title": "",
            "sort_column": "created_at",
            "sort_order": "ASC",
            "category_type": []
            }
