import requests
from WebCases.Payloads import TC01
from WebCases.Links import Link
session = requests.session()


def test_LogIn():
    rLoginStudent = session.post(Link.test_Links(),
                                 json=TC01.correctCredentials())
    jLogIn = rLoginStudent.json()
    logInStatusCode = rLoginStudent.status_code

    print(jLogIn, logInStatusCode)


def test_LogIn2():
    rLoginStudent = session.post(Link.test_Links(),
                                 json=TC01.wrongPassword())
    jLogIn = rLoginStudent.json()
    logInStatusCode = rLoginStudent.status_code
    print(jLogIn, logInStatusCode)


def test_LogIn3():
    rLoginStudent = session.post(Link.test_Links(),
                                 json=TC01.wrongUserName())
    jLogIn = rLoginStudent.json()
    logInStatusCode = rLoginStudent.status_code
    print(jLogIn, logInStatusCode)


def test_LogIn4():
    rLoginStudent = session.post(Link.test_Links(),
                                 json=TC01.myAccount())
    jLogIn = rLoginStudent.json()
    logInStatusCode = rLoginStudent.status_code
    print(jLogIn, logInStatusCode)
