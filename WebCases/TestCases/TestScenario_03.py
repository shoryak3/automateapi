import requests
from WebCases.Payloads import TC03
from WebCases.Links import Link
session = requests.session()


def test_LogIn():
    rLoginStudent = session.post(Link.test_Links(),
                                 json=TC03.test_LogIn())
    jLogIn = rLoginStudent.json()
    sMessage = jLogIn['status']
    if sMessage == '1':
        logInStatusCode = rLoginStudent.status_code
        print(logInStatusCode)


def test_FetchStudentReportCard():
    rFetchStudentReportCard = session.post(Link.test_Links(),
                                           json=TC03.test_ReportCard())
    sMessage = rFetchStudentReportCard.json()
    # rMessage = sMessage['message']
    # print(rFetchStudentReportCard.status_code)
    # print(rMessage)
    print(sMessage)


def test_FetchStudentDetails():
    rFetchStudentDetails = session.post(Link.test_Links(),
                                        json=TC03.test_FetchCourses())
    sMessage = rFetchStudentDetails.json()
    print(sMessage)
