import requests
from WebCases.Payloads import TC04
from WebCases.Links import Link
session = requests.session()


def test_LogIn():
    rLoginStudent = session.post(Link.test_Links(),
                                 json=TC04.test_Login())
    jLogIn = rLoginStudent.json()
    sMessage = jLogIn['status']
    if sMessage == '1':
        logInStatusCode = rLoginStudent.status_code
        print(logInStatusCode)


def test_FetchArticles():
    rFetchArticles = session.post(Link.test_Links(),
                                  json=TC04.test_FetchArticles())
    jFetchArticles = rFetchArticles.json()
    sMessage = jFetchArticles['message']
    print(sMessage)


def test_FetchArticleTags():
    rFetchArticleTags = session.post(Link.test_Links(),
                                     json=TC04.test_FetchArticleTags())
    jFetchArticleTags = rFetchArticleTags.json()
    sMessage = jFetchArticleTags['message']
    print(sMessage)
